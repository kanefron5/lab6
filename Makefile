all: clean main

clean:
	rm -f main *.o o_24.bmp

main:
	gcc -c main.c bmp.c -pedantic -Wall -Werror
	gcc -o main main.o bmp.o -lm
	rm -f *.o o_24.bmp