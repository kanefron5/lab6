//
// Created by Роман Заболотских on 18.12.2020.
//

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "bmp.h"


void println(const char *argv) {
    printf("%s\n", argv);
}

int main(int args, char *argv[]) {

    if (args < 3) {
        println("Использование: `./main pathIn.bmp pathOut.bmp`");
        return 0;
    }

    printf("Исходный файл: %sn\n", argv[1]);

    struct image img, rotatedImage;
    img.data = NULL;
    rotatedImage.data = NULL;

    FILE *imageFile = fopen(argv[1], "rb");
    FILE *rotatedImageFile = fopen(argv[2], "wb");

    if (imageFile == NULL) {
        println("Ошибка считывания файла");
        return 0;
    } else {
        enum read_status readStatus = readImage(imageFile, &img);
        println(read_message[readStatus]);
        if(readStatus != READ_OK){
            goto exit;
        }
        rotatedImage = rotateImage90(img);

        enum write_status writeStatus = writeImage(rotatedImageFile, rotatedImage);
        println(write_message[writeStatus]);
        if(writeStatus != WRITE_OK){
            goto exit;
        }
    }

    exit:
    if (rotatedImage.data) free(rotatedImage.data);
    if (img.data) free(img.data);
    fclose(rotatedImageFile);
    fclose(imageFile);

    return 0;
}

