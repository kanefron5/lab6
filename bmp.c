//
// Created by Роман Заболотских on 18.12.2020.
//

#include "bmp.h"

bool createImage(struct image *const img, uint64_t width, uint64_t height) {
    img->width = width;
    img->height = height;
    img->data = malloc(width * height * sizeof(struct pixel));
    if (img->data == NULL) {
        return false;
    }
    return true;
}


enum read_status readImage(FILE *imageFile, struct image *const img) {
    struct bmp_header header;

    if (!fread(&header, sizeof(header.bfType), 1, imageFile)) return READ_INVALID_HEADER;
    if (fseek(imageFile, 0, SEEK_END)) return READ_INVALID_HEADER;
    uint32_t file_size = ftell(imageFile);
    if (fseek(imageFile, sizeof(header.bfType), SEEK_SET))return READ_INVALID_HEADER;
    if (file_size < sizeof(header))return READ_INVALID_HEADER;

    if (!fread(&header.bfSize, (char *) &header.biWidth - (char *) &header.bfSize, 1, imageFile))
        return READ_INVALID_HEADER;

    if (header.bfSize != file_size) return READ_INVALID_HEADER;
    if (header.biSize < 40) return READ_INVALID_HEADER;
    fread(&header.biWidth, 40 - sizeof(header.biSize), 1, imageFile);
    uint32_t rowSizeWithTrash = (header.biWidth * 3) + (header.biWidth % 4);

    if (!createImage(img, header.biWidth, header.biHeight)) return READ_INVALID_HEADER;
    size_t padding = rowSizeWithTrash - header.biWidth * 3;

    for (size_t row = 0; row < header.biHeight; ++row) {
        if (fread(img->data + row * header.biWidth, sizeof(struct pixel), header.biWidth, imageFile) < header.biWidth)
            return READ_INVALID_BITS;
        if (fseek(imageFile, padding, SEEK_CUR)) return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status writeImage(FILE *out, struct image const img) {
    uint32_t rowSizeWithTrash = (img.width * 3) + (img.width % 4);
    size_t padding = rowSizeWithTrash - sizeof(struct pixel) * img.width;
    uint32_t data_size = rowSizeWithTrash * img.height;

    struct bmp_header header;
    header.bfType = 0x4D42;
    header.bfSize = sizeof(header) + data_size;
    header.bfReserved = 0;
    header.bfOffBits = sizeof(header);
    header.biSize = 40;
    header.biWidth = img.width;
    header.biHeight = img.height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = data_size;
    header.biXPelsPerMeter = 2800;
    header.biYPelsPerMeter = 2800;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (!fwrite(&header, sizeof(header), 1, out)) return WRITE_HEADER_ERROR;

    uint32_t temp_value = 0;
    for (size_t row = 0; row < img.height; ++row) {
        if (fwrite(img.data + row * img.width, sizeof(struct pixel), img.width, out) < img.width)
            return WRITE_HEADER_ERROR;
        if (fwrite(&temp_value, 1, padding, out) != padding)
            return WRITE_ERROR;
    }
    return WRITE_OK;
}

struct image rotateImage90(struct image const img) {
    struct image new_img;
    createImage(&new_img, img.height, img.width);

    for (int row = 0; row < img.height; ++row) {
        for (int column = 0; column < img.width; ++column) {
            new_img.data[column * img.height + (img.height - row - 1)] = img.data[row * img.width + column];
        }
    }
    return new_img;
}


