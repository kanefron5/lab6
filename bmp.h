//
// Created by Роман Заболотских on 18.12.2020.
//
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR
};


static const char *read_message[] = {
        "Файл считан успешно",
        "Ошибка чтения файла",
        "Ошибка заголовка файла"
};

static const char *write_message[] = {
        "Изображение сохранено",
        "Ошибка записи файла",
        "Ошибка записи заголовка"
};


struct bmp_header {
    uint16_t bfType; //Отметка для отличия формата от других (сигнатура формата). Может содержать единственное значение 4D4216/424D16 (little-endian/big-endian).
    uint32_t bfSize; //Размер файла в байтах.
    uint32_t bfReserved; //Зарезервированы и должны содержать ноль
    uint32_t bfOffBits; //Положение пиксельных данных относительно начала данной структуры (в байтах).
    uint32_t biSize; //Размер данной структуры в байтах, указывающий также на версию структуры (см. таблицу версий выше).

    uint32_t biWidth; //Ширина растра в пикселях. Указывается целым числом со знаком. Ноль и отрицательные не документированы.
    uint32_t biHeight; //Целое число со знаком, содержащее два параметра: высота растра в пикселях (абсолютное значение числа) и порядок следования строк в двумерных массивах (знак числа). Нулевое значение не документировано.
    uint16_t biPlanes; //В BMP допустимо только значение 1. Это поле используется в значках и курсорах Windows.
    uint16_t biBitCount; //Количество бит на пиксель (список поддерживаемых смотрите в отдельном разделе ниже).
    uint32_t biCompression; //Указывает на способ хранения пикселей (см. в разделе ниже).
    uint32_t biSizeImage; //Размер пиксельных данных в байтах. Может быть обнулено если хранение осуществляется двумерным массивом.
    uint32_t biXPelsPerMeter; //Количество пикселей на метр по горизонтали и вертикали (см. раздел «Разрешение изображения» данной статьи).
    uint32_t biYPelsPerMeter; //Количество пикселей на метр по горизонтали и вертикали (см. раздел «Разрешение изображения» данной статьи).
    uint32_t biClrUsed; //Размер таблицы цветов в ячейках.
    uint32_t biClrImportant; //Количество ячеек от начала таблицы цветов до последней используемой (включая её саму).

} __attribute__((packed));

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    unsigned char b, g, r;
};


enum read_status readImage(FILE *imageFile, struct image *img);

enum write_status writeImage(FILE *out, struct image img);

struct image rotateImage90(struct image img);
